-- Function to replicate each element of the list n times
f :: Int -> [Int] -> [Int]
f _ [] = []
f n (x:xs) = replicate n x ++ f n xs

-- This part handles the input and output.
main :: IO()
main = getContents >>=
      mapM_ print. (\(n:arr) -> f n arr). map read. words



