-- Filter out the even positions in a list
f :: [Int] -> [Int]
f [] = []
f (x:xs) = [x | (x,y) <- zip (x:xs) [0..], y `mod` 2 /= 0]

-- This part deals with the input and output.
main = do
  inputdata <- getContents
  mapM_ (putStrLn. show). f. map read. lines $ inputdata
