-- Create a function that takes an argument and create an array of size equal to
-- argument.
fn 0 = []
fn n = n : fn (n - 1)

-- This part handles the input and output.
main = do
  n <- readLn :: IO Int
  print (length (fn (n)))

