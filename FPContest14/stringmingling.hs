-- Function to mingle two strings, such that first character of first string
-- is followed by first character of second string and so on.
fnc p q = concat [[p,q] | (x,y) <- zip p q]

-- This part deals with the input and output.
main = do
  p <- getLine
  q <- getLine
  putStrLn $ fnc p q

