-- Function to sum all the odd elements in a list
f xs = sum $ filter (\x -> x `mod` 2 /= 0) xs

-- This part handles the input and output.
main = do
  inputdata <- getContents
  putStrLn $ show $ f $ map (read :: String -> Int) $ lines inputdata

