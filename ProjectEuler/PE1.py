def func(n):
    return sum([i for i in xrange(n) if i%3==0 or i%5==0])


t = int(raw_input())
while t > 0:
    n = int(raw_input())
    print func(n)
    t -= 1
