import Data.List


solve msg = concat . map (\s-> stringify (head s) (length s)) . group $ msg
  where
    stringify ch n
      | n == 1 = [ch]
      | otherwise = [ch] ++ show n

main :: IO ()
main = getContents >>= putStrLn . solve
