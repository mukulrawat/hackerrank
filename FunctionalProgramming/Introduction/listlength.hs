-- Function to find list of length
len :: [a] -> Int
len = foldr (\x acc -> 1 + acc) 0

-- part for Input and output
main = do
  inputdata <- getContents
  putStrLn $ show $ len $ map (read :: String -> Int) $ lines inputdata

