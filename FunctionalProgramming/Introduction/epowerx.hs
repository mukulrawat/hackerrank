roundTo x n = (fromInteger $ round $ x * (10^n)) / (10.0^^n)

factorial :: (Num a, Eq a, Enum a) => a -> a
factorial 0 = 1
factorial n = product [1..n]

series _ []   = [0.0]
series n (z:zs) = roundTo (n^z/(fromIntegral $ factorial z)) 4  : series n zs

solve x = roundTo (sum $ series x [0..9]) 4



-- solve x = roundTo (sum $ series x [0..9]) 4

main :: IO ()
main = getContents >>= mapM_ print. map solve. map (read :: String -> Double). tail.words
