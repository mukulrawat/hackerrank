-- Function to reverse a list
rev :: [a] -> [a]
rev = foldr (\x acc -> acc ++ [x]) []

-- This part is for the input and output
main = do
  inputdata <- getContents
  mapM_ putStrLn $ map show $ rev $ map (read :: String -> Int) $ lines inputdata
