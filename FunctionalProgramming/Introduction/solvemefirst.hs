-- Function to add two values
solveMeFirst :: Int -> Int -> Int
solveMeFirst = \a -> (\b -> a + b)

-- This part handles the input and output.
main :: IO()
main = do
  val1 <- readLn
  val2 <- readLn
  let sum = solveMeFirst val1 val2
  print sum

