-- Function that creates a list of "hello world" n times
hello_worlds 0 = [""]
hello_worlds n = ["Hello World"] ++ hello_worlds (n - 1)

-- This part is related to the Input/Output.
main = do
  n <- readLn :: IO Int
  mapM_ putStrLn $ hello_worlds n


