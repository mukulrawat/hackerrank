
removechar _ [] = []
removechar [] (x:xs) = x : removechar [x] xs
removechar ys (x:xs)
  | x `elem` ys = removechar ys xs
  |otherwise = x:removechar (ys ++ [x]) xs

main :: IO ()
main = do
  s <- getLine
  putStrLn $ removechar [] $ s

