-- Function that takes a list and gives absolute value for each element in the list
f [] = []
f (x : xs)
  | x < 0 = (-x) : f xs
  | otherwise = x : f xs

-- This section handles the input/output.
main = do
  inputdata <- getContents
  mapM_ putStrLn $ map show $ f $ map (read :: String -> Int) $ lines inputdata
